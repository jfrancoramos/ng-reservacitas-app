import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  progressValue = 0.0;
  loading=true;
  constructor() { }

  ngOnInit() {
    this.runProgress();
  }

  runProgress() {
    const interval = setInterval(() => {
      this.progressValue += 0.09;
      console.log("Cargando..");
      if (this.progressValue > 1) {
        this.loading = false;
        clearInterval(interval);
      }
    }, 200);
  }
}
