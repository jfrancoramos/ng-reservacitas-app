import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvoiceProductsPageRoutingModule } from './invoice-products-routing.module';

import { InvoiceProductsPage } from './invoice-products.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    InvoiceProductsPageRoutingModule
  ],
  declarations: [InvoiceProductsPage]
})
export class InvoiceProductsPageModule {}
