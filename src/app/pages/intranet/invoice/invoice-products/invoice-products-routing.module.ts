import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoiceProductsPage } from './invoice-products.page';

const routes: Routes = [
  {
    path: '',
    component: InvoiceProductsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoiceProductsPageRoutingModule {}
