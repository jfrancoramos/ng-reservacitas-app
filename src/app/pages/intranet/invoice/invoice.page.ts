import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES_GENERAL, ROUTES_INVOICE } from 'src/app/shared/constants/constant.model';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.page.html',
  styleUrls: ['./invoice.page.scss'],
})
export class InvoicePage implements OnInit {
  cardArray: any[] = [
    {
      icon: '../../../../assets/icon/invoice/venta-icon.svg',
      name: 'Venta',
      identifier: 'InvoiceSales'
    },
    {
      icon: '../../../../assets/icon/invoice/cancel-invoice.svg',
      name: 'Anulación',
      identifier: 'InvoiceAnulacion'
    }
  ]
  sizeImg = false;
  sizeGrid = 6;
  constructor(private router:Router) { }

  ngOnInit() {
  }

  onNavigateTo(card:any){
    const { identifier, name} = card;
    if(identifier === 'InvoiceSales'){
      console.log('aca',ROUTES_GENERAL.invoice+ROUTES_INVOICE.ventas);
      this.router.navigate([ROUTES_GENERAL.invoice+ROUTES_INVOICE.ventas]);
    }
  }

}
