import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SummaryPageRoutingModule } from './summary-routing.module';

import { SummaryPage } from './summary.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { SummaryComponent } from 'src/app/components/summary/summary.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SummaryPageRoutingModule,
    ComponentsModule
  ],
  declarations: [
    SummaryPage,
    SummaryComponent
  ]
})
export class SummaryPageModule {}
