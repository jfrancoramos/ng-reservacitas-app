export const ROUTES_GENERAL = {
    home: 'summary',
    invoice: 'invoice'
}

export const ROUTES_INVOICE = {
    ventas: '/invoice-products'
}