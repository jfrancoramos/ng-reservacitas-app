import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../shared/shared.module';
import { InvoiceSegmentProductsComponent } from './invoice/invoice-segment-products/invoice-segment-products.component';
import { ModalToppingsProductComponent } from './modals/modal-toppings-product/modal-toppings-product.component';
import { ModalPreviewDetailInvoiceComponent } from './modals/modal-preview-detail-invoice/modal-preview-detail-invoice.component';



@NgModule({
  declarations: [
    HeaderComponent,
    InvoiceSegmentProductsComponent,
    ModalToppingsProductComponent,
    ModalPreviewDetailInvoiceComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    IonicModule
  ],
  exports:[
    HeaderComponent,
    InvoiceSegmentProductsComponent
  ]
})
export class ComponentsModule { }
