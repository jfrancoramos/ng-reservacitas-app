import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { element } from 'protractor';
import { ModalPreviewDetailInvoiceComponent } from '../../modals/modal-preview-detail-invoice/modal-preview-detail-invoice.component';
import { ModalToppingsProductComponent } from '../../modals/modal-toppings-product/modal-toppings-product.component';

@Component({
  selector: 'app-invoice-segment-products',
  templateUrl: './invoice-segment-products.component.html',
  styleUrls: ['./invoice-segment-products.component.scss'],
})
export class InvoiceSegmentProductsComponent implements OnInit {
  OfferCategory: { name, status }[] = [
    {
      name: 'Sandwich',
      status: true
    },
    {
      name: 'Bebidas',
      status: false
    },
    {
      name: 'Hamburguesas',
      status: false
    }
  ]

  listProducts: { name: string, category, price:number,extras:boolean }[] = [
    {
      name: 'Hamburguesa Simple',
      category: 'Hamburguesas',
      price: 9,
      extras:true
    },
    {
      name: 'Napolitana',
      category: 'Sandwich',
      price: 14,
      extras:true
    },
    {
      name: 'Chicha',
      category: 'Bebidas',
      price: 2,
      extras:false
    },
  ]
  selectedProducts:any[];
  categorySelected: { name: string, status: boolean };

  selectedOffer:any[] = [];

  constructor(public modalController: ModalController) { }

  ngOnInit() {
    this.categorySelected = this.OfferCategory[0];
    this.getProductList();
  }

  getInfoCategory(category: { name, status }) {
    this.categorySelected = category;
    let index = this.OfferCategory.indexOf(category);
    this.OfferCategory.map(a => a.status = false);
    this.OfferCategory[index].status = true;
    this.getProductList();
  }

  getProductList(){
    this.selectedProducts = this.listProducts.filter(a=>a.category === this.categorySelected.name);
  }

  async openModalToppings(producto){
    if(producto.extras){
      const modal = await this.modalController.create({
        component: ModalToppingsProductComponent,
        initialBreakpoint: 1,
        cssClass:'claseModal',
        breakpoints: [0, 1],
        componentProps: {
          'producto': producto
        }
      });
      modal.onDidDismiss()
        .then((data) => {
          const user = data; // Here's your selected user!
          if(user.data.dismissed){
            console.log('No agrega',data);
          }else{
            console.log('Si agrega',data);
            this.selectedOffer.push(data.data.producto);
            console.log(this.selectedOffer);
          }
  
      });
      return await modal.present();
    }else{
      this.selectedOffer.push(producto);
      
    }

    
  }

  async seeInvoiceDetail(){
    const modal = await this.modalController.create({
      component: ModalPreviewDetailInvoiceComponent,
      initialBreakpoint: 1,
      breakpoints: [0, 1],
      componentProps: {
        'listaPedido': this.selectedOffer
      }
    });
    modal.onDidDismiss()
      .then((data) => {
        const user = data; // Here's your selected user!
        if(user.data.dismissed){

        }else{
          
        }

    });
    return await modal.present();
  }

}
