import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalPreviewDetailInvoiceComponent } from './modal-preview-detail-invoice.component';

describe('ModalPreviewDetailInvoiceComponent', () => {
  let component: ModalPreviewDetailInvoiceComponent;
  let fixture: ComponentFixture<ModalPreviewDetailInvoiceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPreviewDetailInvoiceComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalPreviewDetailInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
