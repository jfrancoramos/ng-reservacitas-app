import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-preview-detail-invoice',
  templateUrl: './modal-preview-detail-invoice.component.html',
  styleUrls: ['./modal-preview-detail-invoice.component.scss'],
})
export class ModalPreviewDetailInvoiceComponent implements OnInit {
  @Input() listaPedido: any[] = [];
  listInvoiceByCategory: any[] = [];
  constructor(public modalController: ModalController) { }

  ngOnInit() {

    this.initial();
  }

  initial() {
    if (this.listaPedido.length > 0) {
      this.getListByCategory();     
      console.log(this.listInvoiceByCategory);
    }
  }
  getListByCategory() {
    this.listaPedido.reduce((res, value)=> {
      if (!res[value.category]) {
        let productos = this.listaPedido.filter(a=>a.category === value.category)
        res[value.category] = {category:value.category, items: productos};
        this.listInvoiceByCategory.push(res[value.category])
      }
      return res;
    }, {});
  }

  removeExtra(index,index1,tipo:number,index2){
    if(tipo == 1){
      this.listInvoiceByCategory[index].items[index1].salsas.splice(index2,1);
    }else{
      this.listInvoiceByCategory[index].items[index1].otros.splice(index2,1);
    }
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
