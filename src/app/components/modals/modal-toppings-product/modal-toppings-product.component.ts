import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-toppings-product',
  templateUrl: './modal-toppings-product.component.html',
  styleUrls: ['./modal-toppings-product.component.scss'],
})
export class ModalToppingsProductComponent implements OnInit {
  listaSalsas: any[] = [
    {
      nombre:'Ají',
      activo:false
    },
    {
      nombre:'Mayo',
      activo:false
    },
    {
      nombre:'Mtaza',
      activo:false
    },
    {
      nombre:'Chimi',
      activo:false
    },
    {
      nombre:'Kchup',
      activo:false
    }
  ]

  listaOtros: any[] = [
    {
      nombre:'Lechuga',
      activo:false
    },
    {
      nombre:'Tomate',
      activo:false
    }
  ]
s
  @Input() producto;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
    console.log(this.producto);
  }


  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  addProduct(){
    let producto:any = {};
    producto.category = this.producto.category;
    producto.name = this.producto.name;
    producto.salsas = this.listaSalsas.filter(a=>a.activo);
    producto.otros = this.listaOtros.filter(a=>a.activo);

    this.modalController.dismiss({
      producto:producto
    })
  }

}
