import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES_GENERAL } from 'src/app/shared/constants/constant.model';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  business = {
    name:'El Che Carreta',
    direccion: 'Plaza las américas'
  }
  constructor(private router:Router) { }

  ngOnInit() {}
  goInvoice(){
    this.router.navigate([ROUTES_GENERAL.invoice]);
  }
}
