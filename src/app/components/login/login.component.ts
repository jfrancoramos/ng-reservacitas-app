import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES_GENERAL } from 'src/app/shared/constants/constant.model';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {}

  goSummary(){
    this.router.navigate([ROUTES_GENERAL.home],{replaceUrl:true})
  }
}
